# Briskoda Theme

This repository contains a theme for [Hugo], based of [Tachyons]

## Using the theme

You can install the theme either as a clone or submodule.

I recommend the latter. From the root of your Hugo site, type the following:

```
$ git submodule add https://gitlab.com/briskoda/hugo-briskoda-theme.git themes/briskoda
$ git submodule init
$ git submodule update
```

Now you can get updates to briskoda in the future by updating the submodule:

```
$ git submodule update --remote themes/briskoda
```

[hugo]: https://gohugo.io
[tachyons]: https://tachyons.io
